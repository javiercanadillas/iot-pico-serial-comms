#!/usr/bin/env python3
import time
import os

serial_connected = 0
if os.path.exists('/dev/tty.usbmodem0000000000001') == True:
    import serial
    ser = serial.Serial('/dev/tty.usbmodem0000000000001', 115200)
    serial_connected = 1
    time.sleep(1)

while True:
    time.sleep(2)
    for x in [0, 1, 't']:
        command = str(x) + "\n"
        ser.write(bytes(command.encode('ascii')))
        if ser.inWaiting() > 0:
            pico_data = ser.readline()
            pico_data = pico_data.decode("utf-8","ignore")
            print (pico_data[:-2])
        time.sleep(1)
# Introduction

The Raspberry Pi Pico is an RP2040-based MCU. It has an internal temperature sensor and and onboard led, and plenty of capacity to run Micropython. For these reasons, the Pico could be used to demonstrate and end to end IoT setup, but it lacks wireless connectivity (no wifi or BLE).

This project demonstrates how to communicate with the Raspberry Pi Pico through serial interface via USB so we can tether the device to another computer (your laptop, another Raspbery Pi...) that could be acting as MQTT client instead.

The project has two files:

- `main_pico.py`: The main Micropython file that sends temperature readings and accepts commands from the serial line to turno the led on and off.
- `main_server.py`: A simple Python program that connects to the Pico via serial interface and sends three alternating commands: `0` (led off), `1` (led on) and `t` (read temperature). On sending each of them, this program reads the response from the Pico, that could be then incorporated in additional logic to integrate updstream (code not implement, like for instance an MQTT client).

# Instructions

## Installing Micropython

Now grab your Pico, and connect it to your PC/Mac **while pressing the BOOTSEL button**. The Pico filesystem should appear mounted as a mass storage device. Download the [Micropython U2F file](https://www.raspberrypi.org/documentation/rp2040/getting-started/static/f70cc2e37832cde5a107f6f2af06b4bc/rp2-pico-20210205-unstable-v1.14-8-g1f800cac3.uf2) to your computer and drag and drop it onto the RPI-RP2 volume. Your Pico will reboot and you're now running Micropython.

## Installing the software in the Pico

Create a virtual environment and install dependencies:

```bash
python3 -m venv venv
pip install -r requirements.txt
```

As part of the dependencies, you should have `rshell` installed. Connect your You can invoke like this:

```shell
rshell -p /dev/tty.usbmodem0000000000001 --buffer-size 512
```

The location of the serial port may change depending on your OS and physical USB port.

Rshell is a shell, wioth commands similar to those of a shell. The Pico filesystem is mounted in `/pyboard`, so you copy the main program there:

```shell
cp main_pico.py /pyboard/main.py
```

Now, invoke the REPL from there just typing `repl`, and press CTRL-D to do a soft reset and CTRL-X to exit from the repl. Type `exit` to exit `rshell` as well.

# Testing serial communications

The Pico should be executing the main.py program now. To test it, run the server program:

```bash
./main_server.py
```

If it doesn't work, replug your Pico and relaunch the `main_server.py`, making sure there are no other processes connected to the Pico via serial from your PC/Mac.

# Todo

- Use threads: the additional core in the Pico could be used to handle serial comms or just report telemetry for an array of sensors.
- Research communication through I2C or SPI if connecting the Pico to another Pi.

# Links

- [Using rshell with the Pico](https://www.twilio.com/blog/programming-raspberry-pi-pico-microcontroller-micropython)
- [Communicating the Pico with a PC using Python](https://www.raspberrypi.org/forums/viewtopic.php?f=146&t=300474)
- [USB Serial Communication using a second processor/thread of the the RP2040](https://www.raspberrypi.org/forums/viewtopic.php?t=302889)
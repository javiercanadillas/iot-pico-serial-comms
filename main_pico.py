import uselect
import sys
import machine
from machine import Pin
import utime
import _thread

# Built-in temperature sensor 
sensor_temp = machine.ADC(4)
conversion_factor = 3.3 / (65535)
# Built-in led
led = Pin(25, Pin.OUT)

def send_temperature():
   while True:
      reading = sensor_temp.read_u16() * conversion_factor
      temperature = 27 - (reading - 0.706)/0.001721
      return temperature

def getConfig():
   # Set up an input polling object
   spoll = uselect.poll()
   # Register the polling object to the Standard Input file
   spoll.register(sys.stdin, uselect.POLLIN)

   # Read 1 byte from stdin
   sch = sys.stdin.read(1) if spoll.poll(0) else None
   spoll.unregister(sys.stdin)
   return sch


if __name__ == '__main__':
   while True:
      new_ch = getConfig()
      if new_ch == None:
         continue
      elif new_ch == 'q':
         # Unregister
         break
      elif new_ch == '0':
         print('Led is OFF')
         led.off()
      elif new_ch == '1':
         print('led is ON')
         led.on()
      elif new_ch == 't':
         print(send_temperature())
      utime.sleep(1)
